﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestIT.Startup))]
namespace TestIT
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
