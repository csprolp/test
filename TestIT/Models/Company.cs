﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestIT.Models
{
    public class Company
    {
        [Key]
        public int CompanyId { get; set; }
        [Display(Name = "Название компании")]
        public string Name { get; set; }
    }
}